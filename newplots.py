import pickle
import numpy as np
import operator
import math
import matplotlib.pyplot as plt
output = open('/home/ruthvik/Desktop/Summer 2017/working-dir/Results/avg_const__weight_dict_focal_only_on225000_time60_mapsrank_5.pkl','rb')
final_weights = pickle.load(output)
output.close()
noOfmaps = len(final_weights.keys())
#output1 = open('/home/ruthvik/Desktop/Summer 2017/working-dir/Results/saeed_weight_dict_focal_off750000_time30_maps.pkl','rb')
#final_weights1 = pickle.load(output1)
#output1.close()


#print final_weights[map10]
#for items in final_weights.keys():
    #print items
    #print final_weights[items][0:25]==final_weights[items][25:50]==final_weights[items][50:75]
    #print final_weights[items][0:25]
    #pass
print "##########################################################################"
#for items in final_weights1.keys():
#    print items
#    print final_weights1[items][0:25]==final_weights1[items][25:50]==final_weights1[items][50:75]
#    print final_weights1[items][0:25]
maps=final_weights.keys()

	
fig, axes = plt.subplots(5, 6, figsize=(8, 8),
                         subplot_kw={'xticks': [], 'yticks': []})
#print axes

fig.subplots_adjust(hspace=0.3, wspace=0.05)


for ax, interp_method in zip(axes.flat, maps):
    #print interp_method
    #x=[-i for i in range(0,25)]
    x = final_weights[interp_method][0:25]
    #y=final_weights1[interp_method][0:25]
    #print interp_method
    #z = map(operator.sub,x,y)
    #print z
    #[(y-np.mean(x))/float(math.sqrt(np.var(x))) for y in x]
    ax.imshow(np.array(x).reshape(5,5),cmap='gray')
    ax.set_title(interp_method.strip('ip->'))

plt.show()
output3 = open('/home/ruthvik/Desktop/Summer 2017/working-dir/Results/avg_const_dict_spikes_per_img_map_only_on225000_time60_mapsrank_0.pkl','rb')
spikes_per_img_map = pickle.load(output3)
output3.close()

spikes_per_img = []

for i in range(0,len(spikes_per_img_map['map1'])):
    a=0
    for items in spikes_per_img_map.keys():
        a+=spikes_per_img_map[items][i]
    spikes_per_img.append(a)
                   

total = float(len(spikes_per_img_map.keys()))*529  #Total Number of Neurons
spikes_per_img_rate = [items/float(total) for items in spikes_per_img] #List of Spikes/Image/Neuron
print spikes_per_img_rate[0:10]


#plt.xlim([min(spikes_per_img)-5, max(spikes_per_img)+5])
#plt.hist(spikes_per_img,bins=35)
plt.xlim([min(spikes_per_img_rate)-0.5, max(spikes_per_img_rate)+0.5])
plt.hist(spikes_per_img_rate,bins=35)
plt.title('Histogram of the spikes per img')
plt.xlabel('Spikes per image per neuron')
plt.ylabel('count')
plt.show()
#Scatter plot of spikes per img VS time
times = [i for i in range(0,len(spikes_per_img))]
plt.scatter(times,spikes_per_img)
plt.xlabel('No.Of Images')
plt.ylabel('Spikes per img')
plt.show()
for items in final_weights.keys():
    print items
    print final_weights[items][0:25]
    print final_weights[items][0:25]==final_weights[items][0:25]
