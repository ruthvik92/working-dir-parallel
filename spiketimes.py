import pickle
import numpy as np
import operator
import math
import matplotlib
matplotlib.rc('xtick', labelsize=10) 
matplotlib.rc('ytick', labelsize=10)
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import inset_axes, InsetPosition
from poolconvclassrndm_ver2 import *
path = '/home/ruthvik/Desktop/SCNFCN_Results/L123stat/v1/'
output = open(path+'spiketimes_pool1_60ts50st60000_Imgs16_maps.pkl','rb')
spiketimes1 = pickle.load(output)
output.close()
reconst = False
ANALYSIS= True
sep=60
path = '/home/ruthvik/Desktop/Focol_Results/Gabors/'
weights1 = open(path+'weights_16_maps.pkl','rb')
weights=pickle.load(weights1)
weights1.close()

path='/home/ruthvik/Desktop/DataSets/DoGdata/Freshdata/'
output = open(path+'train_labels50000_Images.pkl','rb')
labels = pickle.load(output)
output.close()

map1_spikes = spiketimes1['map11']
if(reconst):
    nofrecon=10
    for p in range(nofrecon):
        spikes=[]
        for key in range(len(map1_spikes)):	
            spikes.append([map1_spikes[key][x] for x in range(0,len(map1_spikes[key])) if sep*p<map1_spikes[key][x]<sep+sep*p])
        recon_img = [0]*49
        for i in range(0,len(spikes)):
            if(len(spikes[i])!=0):
                recon_img[i]=1
        recon_img=np.array(recon_img).reshape(7,7)
        plt.imshow(recon_img,cmap='gray',interpolation='none')
        print('label is:{}').format(labels[p])
        plt.show()


poolconv.raster_plot_spike(map1_spikes,'*')
plt.show()
if(ANALYSIS):
    map_hists = {}
    ymax = 0
    for items in spiketimes1.keys():
        map_hists[items]=[]
        for i  in range(len(spiketimes1[items])):
            map_hists[items].append(len(spiketimes1[items][i]))
            a = len(spiketimes1[items][i])
            if(a>ymax):
                ymax = a
                
    rows = 4
    cols = 4
    fig, axes = plt.subplots(rows, cols,figsize=(16, 8),

                             subplot_kw={'xticks': [2*i for i in range(25)], 'yticks': [1000*i for i in range(ymax/900)]})
    fig.subplots_adjust(hspace=0.2, wspace=0.2)
    ax = axes.flat
    #ip = InsetPosition(ax, [0.4, 0.1, 0.3, 0.7])
    for i in range(1,len(ax)+1):
        maps = 'map'+str(i)
        bars = ax[i-1].bar([zx for zx in range(len(map_hists[maps]))], map_hists[maps])
        reds = sorted(range(len(map_hists[maps])), key=lambda po: map_hists[maps][po])[-7:]
        for red in reds:
            bars[red].set_color('r')
        reds.sort()
        ax[i-1].set_title(maps+','+''+'Dominant Neurons:'+str(reds),fontsize=12)
        ax[i-1].set_ylim(0,ymax+5000)
        small_axes = inset_axes(ax[i-1],width="25%",height="25%",loc=1)
        small_axes.tick_params(axis='both', which='major', labelsize=6)
        small_axes.imshow(np.asarray(weights[maps]).reshape(7,7),cmap='gray',interpolation='None')
        #ax[i-1].set_axes_locator(ip)
        #ax[i-1].imshow(np.asarray(weights[maps]).reshape(7,7),cmap='gray',interpolation='None')
    #plt.tick_params(axis='both', which='major', labelsize=8)   ##doesn't work
    plt.suptitle('Spike profile of maps that are selective to 16 different orientation ')
    plt.show()
        
