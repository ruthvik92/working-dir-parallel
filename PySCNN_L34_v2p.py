"""  code is similar to other PySCoNN's but the weights between ip_pop and "map1","map2".."map4" are 
different and it also plots neuron pots and it also has weight sharing mechanism, this code shares the weights of
fired neuron with other neuorns(corrected the weight sharing here) in the same map by iterating over the number of connections in the projections and also it prints the final and initial values of the evolving weights, here suppression between maps is 
neuron which fired in a map to same neuron in other maps  and also this code 
gives multiple images from the dataset as inputs not the same digit againa nd again as in previous versions
the .pkl file for allspikes is generated using code FinalData/fullspikedataset.py. This code uses customizable input maps
this code doesn't do intra map inhibition as in other codes, and it doesn't do inter map inhibition. trying to make it 
faster than v8, compared to v9, it has OFF center populations also. this code plots spikes per image per map and also
tries to enforce some maximum spikes per map per image as per saeed's suggestions, a map will be kept inactive if the
cumulative activity of the map is >average activity of other maps*homeostasis_const(over some number of images) in order
to keep all the maps in activity. Thi version is same as version 14 but it takes care of updating the weights for only
ON center maps. This removes some redundant recording which was done in previous version and it also records population
acivity for the first 500 images middle 500 images and last 500 images, this version doesn't use the
get_spike_time_ids() function which was added to pyNN core instead it sets the membrane potential of spiked neurons to
-infnitiy whihc prevents from spiking again during that image, before presenting with new image the membrane potnetials
of all the neurons are reset to resting potential. This is parallel version with spedup idea
This version uses syanpses to update weights, this is ap rallel version of working-dir/PySCNN_L34_v1.py"""

import pyNN.neuron as p
from pyNN.utility import ProgressBar
from poolconvclassrndm_ver2 import *
import numpy as np
import numpy
import random
from time import strftime, gmtime
from pyNN.utility import get_simulator, init_logging, normalized_filename
from pyNN.utility.plotting import DataTable
from pyNN.parameters import Sequence
from pyNN.utility.plotting import Figure, Panel
from copy import deepcopy
import sys
import pickle
#import progressbar
import timeit
import operator
from pyNN import recording
from pyNN.common import control
#from mpi4py import MPI
#comm = MPI.COMM_WORLD
#start_time = timeit.default_timer()
comm,dum,ANY_SOURCE = recording.get_mpi_comm()
#from mpi4py.MPI import ANY_SOURCE

delay=1
rank = comm.Get_rank()
size = comm.Get_size()
#min_delay=1

nofMaps= 60
kernel = 5
separation = 15
nofImages= 5
homeostasis_const= 10
activity= 0
A_plus = 0.04
A_minus = 0.03
cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 0.1,      #The refractory period in ms
                   'tau_syn_E' : 5,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 5,    #The inhibitory input current decay time-constant
                   'v_reset'   : -65,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -55  #The threshold voltage at which the neuron will spike.
                   }

p.setup(timestep=0.5,min_delay=1)#min_delay=1.25
comn_path= '/home/ruthvik/Desktop/Summer 2017/working-dir/Results/'
output = open(comn_path+'spiketimes_focal_only_on375000_time30_mapsrank_0.pkl' ,'rb')
allspikes = pickle.load(output)
output.close()
image_size= int(math.sqrt(len(allspikes['map1'])))
ip_Neurons = int(image_size*image_size) #New_image size is sqrt(no of neurons in prev layer)
prev_nofMaps = len(allspikes)
op_Neurons = ip_Neurons-kernel+1
ip_spikes=[]
for i in range(0,len(allspikes.keys())):
    #print i
    ip_spikes.append(allspikes['map'+str(i+1)])
#t_stop =int( max([max(items) for items in allspikes if len(items)!=0])+5)
t_stop = separation*nofImages
print "TIME NEEDED FOR SIMULATION",t_stop
print 'length of ip_spikes',len(ip_spikes)

stimlus_pop=p.Population((11,11,30),p.SpikeSourceArray(spike_times=ip_spikes),label='stimulus')
p.end()
