
"""In this code there is no learning it just forms L1(1map)->L2(30maps,convolution)->L3(30maps,pooling) and records
spiketimes for L3.  In this code neuron voltages for every image is reset unline in v1."""

import pyNN.neuron as p
from pyNN.utility import ProgressBar
from poolconvclassrndm_ver2 import *
import numpy as np
import random
from time import strftime, gmtime
from pyNN.utility import get_simulator, init_logging, normalized_filename
from pyNN.utility.plotting import DataTable
from pyNN.parameters import Sequence
from pyNN.utility.plotting import Figure, Panel
from copy import deepcopy
import sys
import pickle
import progressbar
import timeit
import operator
from pyNN import recording
from pyNN.common import control
comm,dum,ANY_SOURCE = recording.get_mpi_comm()
delay=1
rank = comm.Get_rank()
size = comm.Get_size()
#min_delay=1

nofMaps= 60
kernel = 5
separation = 15
nofImages= 500
pool_kernel = 7

cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 0.1,      #The refractory period in ms
                   'tau_syn_E' : 5,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 5,    #The inhibitory input current decay time-constant
                   'v_reset'   : -65,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -58  #The threshold voltage at which the neuron will spike.
                   }

p.setup(timestep=0.5,min_delay=1)#min_delay=1.25

############################## Loading the input spike file for L3########################################
output = open('/home/ruthvik/Desktop/Summer \
2017/working-dir/Results/man_1spk_pool_spiketimes_focal_only_on375000_time30_mapsrank_0.pkl','rb')
allspikes = pickle.load(output)
###Since Neuron and PyNN have an issue with min_delay and timestep, converted all 1ms timestep to 0.5ms by divind it by2
output.close()
for items in allspikes.keys():
    allspikes['prev_'+str(items)]=allspikes.pop(items)


############################# Loading the trained weight file for L3-L4####################################
weights1 = open('/home/ruthvik/Desktop/Summer 2017/working-dir/Focol_Results/6.5kImgsON_AVG_CONST_10_rate_0.04_0.03_para_4c_L4L5_vth_55/avg_const__weight_dict_focal_only_on97500_time60_mapsrank_0.pkl','rb')
weights=pickle.load(weights1)
weights1.close()

#############################Calculate number of neurons and maps in L3####################################
image_size= int(math.sqrt(len(allspikes['prev_map1'])))
Neurons = int(image_size*image_size) #New_image size is sqrt(no of neurons in prev layer)
prev_nofMaps = len(allspikes)

#############################Calculate the total biological time for the sim##############################
#t_stop =int( max([max(items) for items in allspikes if len(items)!=0])+5)
t_stop = separation*nofImages
print "TIME NEEDED FOR SIMULATION",t_stop

############################Create the labels for the input population(L4)##############################
pops = []
for i in range(1,nofMaps+1):
    pops.append("map"+str(i))

###########################Create the labels for the stimulus population(L3)#############################
prev_pops =[]
for i in range(1,prev_nofMaps+1):
    prev_pops.append("prev_map"+str(i))

###########################Initialize kernels for connection L3 population with L4##########################
kernels ={}
for things in pops:
    for items in prev_pops:
        z = poolconv(kernel,image_size,0.8,0.05,1.0,kernel-1)
        z.PoolConv()
        kernels[items+'->'+things] =  z.conn_list

##########################Overwrite the initialized kenels with learned kernels#############################
for maps in weights.keys():
    map_weights=weights[maps][0:kernel**2]*(len(kernels[maps])/kernel**2)
    for i in range(0,len(kernels[maps])):
        lst_weight =list(kernels[maps][i])
        lst_weight[2]=map_weights[i]
        tuple_weight = tuple(lst_weight)
        kernels[maps][i]=tuple_weight



#########################Initialize the pooling kernels for L4 to L5########################################
pool_kernels ={}
for i in range(0,len(pops)):
    pool_z = poolconv(pool_kernel,image_size-kernel+1,1,0,1.0,0)
    pool_z.PoolConv()
    pool_kernels[pops[i]] =  pool_z.conn_list 


#########################Creating the stimulus population L3###############################################
stimlus_pop = {}
for items in prev_pops:
	stimlus_pop[items]=p.Population(Neurons,p.SpikeSourceArray(spike_times=\
    allspikes[items]),label=items)


#########################Creating the Input populations L4################################################
populations ={}
for i in range(0,len(pops)):
	populations[pops[i]] =  p.Population((image_size-kernel+1)**2, p.IF_curr_exp,cell_params_lif, label=pops[i])


#########################Creating the Pooling population L5###############################################
pool_populations ={}
for i in range(0,len(pops)):
	pool_populations[pops[i]] =  p.Population(((image_size-kernel+1)/pool_kernel)**2, p.IF_curr_exp,cell_params_lif, label=pops[i])

progress_bar = ProgressBar(width=60)

#########################Creating the connectors for L3 to L4############################################
ip_map_connector ={}
for things in pops:
    for items in prev_pops:
        ip_map_connector[things+str("_listconn_")+items]= p.FromListConnector(kernels[items+'->'+things], safe = True, callback = progress_bar,column_names=["weight", "delay"])

#########################Creating the connectors for L4 to L5############################################
pool_map_connector ={}
for i in range(0,len(pops)):
    pool_map_connector[str("listconn_")+pops[i]]= p.FromListConnector(pool_kernels[pops[i]], safe = True, callback = progress_bar,column_names=["weight", "delay"])

###########################Create a dictionary of useful synapses ######################################
synapse_types={
               "static":p.StaticSynapse(weight=8, delay=1),
               "static_inh":p.StaticSynapse(weight=-1.0, delay=1),
               "custom":p.StaticSynapse(delay=1)}

##########################Creating the projections from L3 to L4 #######################################
projections = {}
for things in pops:
    for items in prev_pops:
        projections[items+'->'+things] = p.Projection( stimlus_pop[items],populations[things],\
        ip_map_connector[things+str("_listconn_")+items], synapse_type = synapse_types["custom"], receptor_type="excitatory",\
        label=items+'->'+things)
#########################Creating the projections from L4 to L5 ########################################
pool_projections = {}
for i in range(0,len(pops)):
    pool_projections[str("ip->")+pops[i]] = p.Projection(populations[pops[i]],pool_populations[pops[i]],\
    pool_map_connector[str("listconn_")+pops[i]], synapse_type = synapse_types["custom"], receptor_type="excitatory")


for items in populations.keys():
    pool_populations[items].record('spikes')

bar = progressbar.ProgressBar(maxval=t_stop, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
start_time = timeit.default_timer()

class MyReset(object): 
    def __init__(self, sampling_interval, popltns):	
        self.interval = sampling_interval
        self.popltns = popltns 
        self.cell_voltages={}
        self.global_cell_voltages={}
        self.displaces={}
        self.reset_disp={}
        self.all_local_ids = {}
        self.local_ids={}
        self.pool_spiketimes={}
        for items in self.popltns.keys():
            local_ids = self.popltns[items].local_cells
            self.local_ids[items] = local_ids
            all_local_ids,_,_ = recording.allgather_array(np.array(local_ids,dtype = 'float'))
            self.all_local_ids[items] = all_local_ids.tolist()
            self.pool_spiketimes[items] = [[] for i in range(9)]
    def __call__(self,t):
        ##Reset the voltages of neurons after passing an image
        if(t!=0):
            if((t)%separation==0):
                #print 'came here and time is',t
                for items in self.popltns.keys():
                    self.popltns[items].set_membrane_potential([(i,cell_params_lif['v_reset']) for i in \
                    range(0,self.reset_disp[items][comm.rank])])	
		##This pasrt of code is to prevent more than one spike by a neuron in entire duration of an image	
        for items in self.popltns.keys():
            zx= self.popltns[items].get_membrane_potential()
            bn,self.displaces[items], self.reset_disp[items]= recording.allgather_array(np.array(zx))
            self.cell_voltages[items] = zx
            self.global_cell_voltages[items]=bn.tolist()
            spiked_ids=[]
            for ids in range(0,len(self.cell_voltages[items])):
                if(self.cell_voltages[items][ids]>cell_params_lif['v_thresh']):
                    #No need to convert local_ids to global_ids here, just ids is enough.
                    #ids = self.popltns[items].id_to_index(self.local_ids[items][ids])#id_to_index method converts 
                    #local ids to global ids
                    spiked_ids.append(ids)
                    #print('cell {} in {} spiked, voltage {},global id {} rank {},time {}').format(ids,\
                    #items,self.cell_voltages[items][ids],self.popltns[items].id_to_index(self.local_ids[items][ids]),comm.rank,t)
            
            #print('cells {} in {} spiked, rank {},time {}').format(spiked_ids, items, comm.rank,t)
            global_spiked_ids=[]
            for ids in range(0,len(self.global_cell_voltages[items])):
                if(self.global_cell_voltages[items][ids]>cell_params_lif['v_thresh']):
                    ids = self.popltns[items].id_to_index(self.all_local_ids[items][ids])
                    global_spiked_ids.append(ids)
            for nos in global_spiked_ids:
                self.pool_spiketimes[items][nos].append(t)
            
            
            #print('global cells {} in {} spiked rank {} time {}').format(global_spiked_ids, items, comm.rank,t)
            #if(t==10.0):
            #    sys.exit()
            vals_to_set = [(vals, -float('inf')) for vals in spiked_ids]
            self.popltns[items].set_membrane_potential(vals_to_set)	
        if(comm.rank ==0):
            bar.update(t+self.interval-0.5)
        comm.Barrier()
        return t + self.interval

    def GetSpikes(self):
        return self.pool_spiketimes

my_reset = MyReset(sampling_interval=0.5, popltns=pool_populations)
p.run(t_stop,callbacks=[my_reset])
bar.finish()
elapsed = timeit.default_timer()-start_time
print("Total time is {}").format(elapsed)
spikes = my_reset.GetSpikes()
pool_spike_times={}
for pops in pool_populations.keys():
	pool_spike_times[pops]=pool_populations[pops].get_data(gather=True).segments[0].spiketrains

path = '/home/ruthvik/Desktop/Summer 2017/working-dir/Results/'
#picklefile2="auto_1spk_pool_spiketimes_focal_only_on"+str(t_stop)+"_time"+str(nofMaps)+"_maps"+"rank_"+str(comm.rank)+".pkl"
#spike_times = open(path+picklefile2,'wb')
#pickle.dump(pool_spike_times,spike_times)
#spike_times.close()
#print path+picklefile2
if(comm.rank==0):
    picklefile3="man_1spk_pool_spiketimes_focal_only_on"+str(t_stop)+"_time"+str(nofMaps)+"_maps"+"rank_"+str(comm.rank)+".pkl"
    spike_times = open(path+picklefile3,'wb')
    pickle.dump(spikes,spike_times)
    spike_times.close()
    print path+picklefile3
comm.Barrier()
p.end()
