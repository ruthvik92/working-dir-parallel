"""  code is similar to other PySCoNN's but the weights between ip_pop and "map1","map2".."map4" are 
different and it also plots neuron pots and it also has weight sharing mechanism, this code shares the weights of
fired neuron with other neuorns(corrected the weight sharing here) in the same map by iterating over the number of connections in the projections and also it prints the final and initial values of the evolving weights, here suppression between maps is 
neuron which fired in a map to same neuron in other maps  and also this code 
gives multiple images from the dataset as inputs not the same digit againa nd again as in previous versions
the .pkl file for allspikes is generated using code FinalData/fullspikedataset.py. This code uses customizable input maps
this code doesn't do intra map inhibition as in other codes, and it doesn't do inter map inhibition. trying to make it 
faster than v8, compared to v9, it has OFF center populations also. this code plots spikes per image per map and also
tries to enforce some maximum spikes per map per image as per saeed's suggestions, a map will be kept inactive if the
cumulative activity of the map is >average activity of other maps*homeostasis_const(over some number of images) in order
to keep all the maps in activity. Thi version is same as version 14 but it takes care of updating the weights for only
ON center maps. This removes some redundant recording which was done in previous version and it also records population
acivity for the first 500 images middle 500 images and last 500 images, this version doesn't use the
get_spike_time_ids() function which was added to pyNN core instead it sets the membrane potential of spiked neurons to
-infnitiy whihc prevents from spiking again during that image, before presenting with new image the membrane potnetials
of all the neurons are reset to resting potential. This is parallel version with spedup idea
This version uses the .get to get weights and tried to minimize gets"""

import pyNN.neuron as p
from pyNN.utility import ProgressBar
from poolconvclassrndm_ver2 import *
import numpy as np
import numpy
import random
from time import strftime, gmtime
from pyNN.utility import get_simulator, init_logging, normalized_filename
from pyNN.utility.plotting import DataTable
from pyNN.parameters import Sequence
from pyNN.utility.plotting import Figure, Panel
from copy import deepcopy
import sys
import pickle
#import progressbar
import timeit
import operator
from pyNN import recording
from pyNN.common import control
#from mpi4py import MPI
#comm = MPI.COMM_WORLD
#start_time = timeit.default_timer()
comm,dum,ANY_SOURCE = recording.get_mpi_comm()
#from mpi4py.MPI import ANY_SOURCE

Neurons = image_size*image_size
delay=1
rank = comm.Get_rank()
size = comm.Get_size()
#min_delay=1

nofMaps= 30
kernel = 5
separation = 30
nofImages= 15000
homeostasis_const= 50
activity= 0
A_plus = 0.004
A_minus = 0.003
cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 0.1,      #The refractory period in ms
                   'tau_syn_E' : 5,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 5,    #The inhibitory input current decay time-constant
                   'v_reset'   : -65,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -55  #The threshold voltage at which the neuron will spike.
                   }

p.setup(timestep=0.5,min_delay=1)#min_delay=1.25

output = open('/home/ruthvik/Desktop/Summer \
2017/working-dir/Focol_Stimulus/stimulus6000030_separationImagesfocol_rate7_width1_sigma2_types1.pkl','rb')
allspikes = pickle.load(output)
output.close()
comm.Barrier()


t_stop =int( max([max(items) for items in allspikes if len(items)!=0])+5)
t_stop = separation*nofImages
#print "TIME NEEDED FOR SIMULATION",t_stop
#4 input map populations.
pops = []
for i in range(1,nofMaps+1):
    pops.append("map"+str(i))

#kerneles will store "nofMaps" different initialized weights for 4 maps
kernels ={}
for i in range(0,len(pops)):
    z = poolconv(kernel,27,0.8,0.05,delay,4)
    z.PoolConv()
    kernels[pops[i]] =  z.conn_list 
print len(kernels['map1']), comm.rank

progress_bar = ProgressBar(width=60)

# Stimulus population, used to connect to input population.(See if you can avoid this)
stimlus_pop = p.Population(Neurons,p.SpikeSourceArray(spike_times= allspikes),label="driver")
#print 'stimlus_pop',len(stimlus_pop), comm.rank
#dictionary of populations for the first stage.
populations ={}
for i in range(0,len(pops)):
	populations[pops[i]] =  p.Population((image_size-kernel+1)**2, p.IF_curr_exp,cell_params_lif, label=pops[i])

#for items in populations.keys():
    #print 'output pops', len(populations[items]), items, comm.rank 

#dictionary of connections from population ip_pop to map1, map2, map3, map4 populations(first stage). 
ip_map_connector ={}
for i in range(0,len(pops)):
    ip_map_connector[str("listconn_")+pops[i]]= p.FromListConnector(kernels[pops[i]], safe = True, callback = progress_bar,column_names=["weight", "delay"])

#dictionary of synapse types.
synapse_types={
               "custom":p.StaticSynapse(delay=delay)}
#projection of input population to various maps on the first stage.
#print("********Projecting input population to the {}  maps of first stage******").format(nofMaps)
projections = {}
for i in range(0,len(pops)):
	projections[str("ip->")+pops[i]] = p.Projection( stimlus_pop,populations[pops[i]],\
    ip_map_connector[str("listconn_")+pops[i]], synapse_type = synapse_types["custom"], receptor_type="excitatory")

#for items in projections.keys():
    #print 'length of projections', len(projections[items]), items, comm.rank



#print("********Done, Projecting input population to the {} maps of first stage******").format(nofMaps)
#bar = progressbar.ProgressBar(maxval=t_stop, \
    #widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
#bar.start()
#print "Progress of Simulation"
for items in populations.keys():
    populations[items].record('spikes','v')
#print control.build_state_queries().get_min_delay()
start_time = timeit.default_timer()
class SparseCoding(object):
    """
    This class implements sparse coding algorithm as in Saeed's paper, There is lateral inhibition mechanism in all
    convolutionaal layers. When a neuron fires in a specific location, it inhibits other neurons in that location
    belonging to other neuronal maps and does not allow them to fire in the following time steps. In addition neurons
    are not allowed to fire more than once. A winner neuron in a map prevents other neurons in its own map to do STDP
    and duplicates its updated synaptic weights into them. Also, there is a local inter-map competition for STDP. When
    a neuron is allowed to do STDP, it prevents the neurons in other maps within a small neighbourhood around its
    location from doing STDP. THis is cruicial to encourage neurons of different maps to learn different features. 
    self.cell_voltages contains neuron voltages in a map for a time step, self.tntatv_stdp_ids contains neuron ids of
    various maps on which STDP is to be done, self.neur_ids stores all the neurons where STDP was performed and these
    neurons won't get a chance to update till the next image(list resets for ever image), self.spikes_per_img is number
    of STDPs done for one image, self.spike_rate stores the previous self.spikes_per_img value for all images."""

    def __init__(self, sampling_interval, prjctions,popltns):
        self.interval = sampling_interval
        self.prjctions = prjctions
        self.popltns = popltns
        self.cell_voltages = {}
        self.global_cell_voltages={}
        self.tntatv_stdp_ids={}
        self.global_tntatv_stdp_ids={}
        self.spikes_per_img_map_rate ={} #This is time series data of the next variable.
        self.spikes_per_map_img={}  #This is number of updates per map per img.
        for items in self.popltns.keys():
            self.spikes_per_img_map_rate[items]=[]            
            self.spikes_per_map_img[items]=0
        self.all_local_ids = {}
        self.local_ids = {}
        self.activities={}
        for items in self.popltns.keys():
            self.activities[items]=[]
            local_ids = self.popltns[items].local_cells
            self.local_ids[items] = local_ids
            all_local_ids,_,_ = recording.allgather_array(numpy.array(local_ids,dtype = 'float'))
            self.all_local_ids[items] = all_local_ids.tolist() 
        self.displaces = {}
        self.reset_disp = {} 
        self.lcl_syns = {}
        for items in self.prjctions.keys():
            weights=self.prjctions[items].get('weight',format='list',with_address=True,gather=False)
            #Sort the weights according to post syn neuron id
            weights.sort(key=lambda tup: tup[1])
            #Get all the IDs of post syn neurons in the weights acquired above.
            self.lcl_syns[items] = [weights[i][1] for i in range(0,len(weights))]


    def __call__(self, t):
        #Calculate the no.of input
         
        time_bin = int(t)/separation
        #Reset the threshold of populations because they might be set to infinity as per algorithm in the previous input
        if((t+1)%separation==0):
            #resetting the pool of previous updated neurons.
            for items in self.popltns.keys():
                self.popltns[items].set_membrane_potential([(i,cell_params_lif['v_reset']) for i in \
                range(0,self.reset_disp[items][comm.rank])])
                self.spikes_per_img_map_rate[items].append(self.spikes_per_map_img[items])
                self.spikes_per_map_img[items]=0
            
        if(time_bin>=homeostasis_const-1):
            cum_sum={}
            excludes=[]
            othr_map_spks=0
            for items in self.spikes_per_img_map_rate.keys(): 
                cum_sum[items]=sum(self.spikes_per_img_map_rate[items][-homeostasis_const:])
                other_maps = list(set(self.spikes_per_img_map_rate.keys())-set([items]))
                for other_items in other_maps:
                    othr_map_spks+=sum(self.spikes_per_img_map_rate[other_items][-homeostasis_const:])
                if cum_sum[items]>othr_map_spks/len(other_maps):
                    excludes.append(items)
            
            for items in excludes:
                #start_time_2 = timeit.default_timer()
                weights=self.prjctions[str("ip->")+items].get('weight',format='list',with_address=True,gather=True)[((kernel)**2*0):(kernel**2*(0+1))]
                
                weights.sort(key=lambda tup: tup[0])
                zebra  = [] 
                for i in range(0,len(weights)):
                    lst_weights = list(weights[i])
                    lst_weights[2]=lst_weights[2]-A_minus*lst_weights[2]*(1-lst_weights[2])
                    zebra.append(lst_weights[2])
                

                zebras=zebra*(len(list(self.prjctions[str("ip->")+items].connections))/kernel**2)
                synapses =list(self.prjctions[str("ip->")+items].connections)
                #print [synapses[x].weight for x in range(0,50)]
                #sys.exit()
                #print len(synapses), comm.rank
                for  c in range(0,len(zebras)):
                    synapses[c].weight = zebras[c]
                #elapsed2 = timeit.default_timer()-start_time_2
                #print("Total time is {}").format(elapsed2)

            

        #Uncomment next line to see the global id of the neuron in that population
        #if neuron 729 is absent in that population, expect an error.
        #print self.popltns['map1'].id_to_index(729)
        #uncomment the next few lines to see the local_ids of map2 which are on
        #process 0. 
        #if(comm.rank==0):
           #print self.popltns['map2'].local_cells, comm.rank, t
            #print '###########################################'
            #print self.all_local_ids['map2'][0:77]        
        for items in self.popltns.keys():
            zx= self.popltns[items].get_membrane_potential()
            bn,self.displaces[items], self.reset_disp[items]= recording.allgather_array(numpy.array(zx))
            self.cell_voltages[items] = zx
            self.global_cell_voltages[items]=bn.tolist()
            spiked_ids=[]
            for ids in range(0,len(self.cell_voltages[items])):
                if(self.cell_voltages[items][ids]>cell_params_lif['v_thresh']):
                    #No need to convert local_ids to global_ids here, just ids is enough.
                    #ids = self.popltns[items].id_to_index(self.local_ids[items][ids])
                    spiked_ids.append(ids)
            global_spiked_ids=[]
            for ids in range(0,len(self.global_cell_voltages[items])):
                if(self.global_cell_voltages[items][ids]>cell_params_lif['v_thresh']):
                    ids = self.popltns[items].id_to_index(self.all_local_ids[items][ids])
                    global_spiked_ids.append(ids)
            
            # if (0<time_bin<activity or nofImages/2-activity/2<time_bin<nofImages/2+activity/2 or \
            # nofImages-activity<time_bin<nofImages):

            #     zero_activity = [0 for i in range(0,len(self.popltns[items]))]
            #     for values in global_spiked_ids:
            #         zero_activity[values]=1
            #     self.activities[items].append(zero_activity)

            self.spikes_per_map_img[items]+=len(global_spiked_ids)
            self.tntatv_stdp_ids[items]=global_spiked_ids
            
            vals_to_set = [(vals, -float('inf')) for vals in spiked_ids]
            self.popltns[items].set_membrane_potential(vals_to_set)
        
        start_time_1 = timeit.default_timer()
        a_keys = self.tntatv_stdp_ids.keys()

        #Filter tentative neuron ids using suppression algo to come up with final stdp neuron ids. 
        for i in range(0,len(a_keys)):
            b_keys= list(set(a_keys) - set([a_keys[i]]))
            c_keys = self.tntatv_stdp_ids[a_keys[i]]
            for j in range(0,len(b_keys)):
                d_keys=self.tntatv_stdp_ids[b_keys[j]]
                for k in c_keys[:]:
                    key = k
                    key_row= key/(image_size-kernel+1)
                    key_col = key%(image_size-kernel+1)
                    remove =0
                    for l in d_keys[:]:
                        target = l
                        tar_row = target/(image_size-kernel+1)
                        tar_col = target%(image_size-kernel+1)
                        
                        if(abs(key_row-tar_row) > kernel-1 and abs(key_col-tar_col) > kernel-1):
                            pass
                        else:
                            if(self.global_cell_voltages[a_keys[i]][key]>=self.global_cell_voltages[b_keys[j]][target]):
                                d_keys.remove(target)
                            else:
                                remove+=1
                    if(remove):
                        c_keys.remove(key)
        
        #elapsed1 = timeit.default_timer()-start_time_1

        #if(comm.rank==0):
            #print("time for competition {}").format(elapsed1), comm.rank
            
            #print 'elapsed1 cum', elapsed1

        # Apply STDP for the filtered tntatv_stdp_ids neuron ids.()
        for items in self.tntatv_stdp_ids.keys():
            #weights=self.prjctions[str("ip->")+items].get('weight',format='list',with_address=True,gather=False)
            #Sort the weights according to post syn neuron id
            #weights.sort(key=lambda tup: tup[1])
            #Get all the IDs of post syn neurons in the weights acquired above.
            #lcl_syns = [weights[i][1] for i in range(0,len(weights))]
            a = len(self.tntatv_stdp_ids[items])
            np_zebra = np.empty(kernel**2, dtype='float')
            if(a!=0):
                volts = [self.global_cell_voltages[items][n] for n in self.tntatv_stdp_ids[items]]
                max_volt = max(volts)
                neur_id = self.tntatv_stdp_ids[items][volts.index(max_volt)]
                #if the winner neuron ID is in post syn IDs of this node update 
                #it's weights
                if neur_id in self.lcl_syns[str("ip->")+items]:
                    
                    weights=self.prjctions[str("ip->")+items].get('weight',format='list',with_address=True,gather=False)
                    
                    weights.sort(key=lambda tup: tup[1])
                    #uncomment the next line to see neur_id of post_syn neuron for which
                    #weights are being updated.
                    #print 'post syn neuron id',neur_id, comm.rank, items, t
                    grp_id = self.lcl_syns[str("ip->")+items].index(neur_id) #Find out at what index syns of 
                    #the post syn neuron start?
                    weights = weights[grp_id:kernel**2+grp_id]
                    #uncomment the next line to see the weights of post_syn neuron
                    #which was chosen to be updated.
                    #print 'weights, maps, time', weights, items, comm.rank, t
                    zebra = []
                    for i in range(0,len(weights)):
                        key = weights[i][0]
                        Q = [allspikes[key][x] for x in range(0,len(allspikes[key])) if separation*time_bin\
                        <allspikes[key][x] <t]
                        if len(Q)!=0:
                            lst_weights = list(weights[i])
                            lst_weights[2]=lst_weights[2]+A_plus*lst_weights[2]*(1-lst_weights[2])
                            zebra.append(lst_weights[2])
                        else:
                            lst_weights = list(weights[i])
                            lst_weights[2]=lst_weights[2]-A_minus*lst_weights[2]*(1-lst_weights[2])
                            zebra.append(lst_weights[2])

                    np_zebra = np.array(zebra,dtype='float')
                    np_zebras=np_zebra.tolist()*(len(list(self.prjctions[str("ip->")+items].connections))/kernel**2)
                    synapses =list(self.prjctions[str("ip->")+items].connections)
                    for  c in range(0,len(np_zebras)):
                        synapses[c].weight = np_zebras[c]
                    all_ranks = [l for l in range(0,size)]
                    all_ranks.remove(rank)
                    for ranks in all_ranks:
                        if(sum(np_zebra)==0):
                            sys.exit()
                        comm.Send(np_zebra, dest=ranks)
                        #print 'process, weights, map, time', comm.rank, np_zebra, items, t
                        #sys.stdout.flush()
                else:
                    comm.Recv(np_zebra,source=ANY_SOURCE)
                    np_zebras=np_zebra.tolist()*(len(list(self.prjctions[str("ip->")+items].connections))/kernel**2)
                    
                    synapses =list(self.prjctions[str("ip->")+items].connections)
                    for  c in range(0,len(np_zebras)):
                        synapses[c].weight = np_zebras[c]
                    
                    #print 'received process, weights, map, time', comm.rank, np_zebra, items, t
                    #sys.stdout.flush()
                #sys.exit()
        #elapsed2+=elapsed2
        #elapsed2 = timeit.default_timer()-start_time_2
        #if(comm.rank==0):
            #print("time for 1 timestep {}").format(elapsed2), t
            
            #print 'elapsed2 cum', elapsed2
        comm.Barrier()
        return t + self.interval 

    def get_spikes_per_img_map(self):
        return self.spikes_per_img_map_rate

    #def get_spike_activities(self):
        #return self.activities
sparse_coding = SparseCoding(sampling_interval=0.5, prjctions=projections, popltns = populations)

p.run(t_stop, callbacks=[sparse_coding])
#bar.finish()

elapsed = timeit.default_timer()-start_time
print("Total time is {}").format(elapsed)

spikes_per_img_map = sparse_coding.get_spikes_per_img_map()

avg=0
for items in spikes_per_img_map.keys():
    avg+=sum(spikes_per_img_map[items])

print 'Avg No. of Spikes per img',avg/float(nofImages)
final_weights = {}  
for items in projections.keys():
    final_weights[items]=[]
    for i in range(0,4):
        qwer = projections[items].get('weight',format='list',with_address=True)[25*i:25*(i+1)]
        qwer.sort(key = lambda tup: tup[0])
        for j in range(0,25):
            final_weights[items].append(qwer[j][2])    

import pickle
picklefile="avg_const__weight_dict_focal_only_on"+str(t_stop)+"_time"+str(nofMaps)+"_maps"+"rank_"+str(comm.rank)+".pkl"
#picklefile1="Map_activities_focal_only_on"+str(t_stop)+"_time"+str(nofMaps)+"_maps"+".pkl"
picklefile3="avg_const_dict_spikes_per_img_map_only_on"+str(t_stop)+"_time"+str(nofMaps)+"_maps"+"rank_"+str(comm.rank)+".pkl"

path = '/home/ruthvik/Desktop/Summer 2017/working-dir/Results/'

output = open(path+picklefile,'wb')
pickle.dump(final_weights,output)
output.close()
#output1 = open(path+picklefile1,'wb')
#pickle.dump(spike_activities,output1)
#output1.close()
output3 = open(path+picklefile3,'wb')
pickle.dump(spikes_per_img_map,output3)
output3.close()
print path+picklefile
#print path+picklefile1
print path+picklefile3
comm.Barrier()
#elapsed = timeit.default_timer()-start_time
#print("Total time is {}").format(elapsed)
p.end()
