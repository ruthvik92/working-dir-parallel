# README 
This is a parallel implementation of PySCNN_v17.py replace site-packages/PyNN/neuron/populations.py with neuron_populations.py and recording/__init__.py with recording__init__.py 
 Don't forget to remove neuron from neuron_populations.py and recording from recording__init__.py
### What is this repository for? ###

* This is the code that I wrote to form Spiking CNN using PyNN and Neuron(later Brian).
* Version =0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
* Install Neuron and Brian softwares, to install Neuron see the doc, "how_to_install_neuron"
* Install PyNN software
* Version 0.1 of this code includes dynamic accessing of state variables(membrane voltage and spike times) for Neuron only
* To access membrane potential and spike times during simualtion do the following:
*Deployment Instructions

Edit the code from dist-packages of python2.7 system libraries
pyNN/neuron/populations.py 
In the Population class add the folowing three methods:

## You can get membrane potential during simulation.
	def get_membrane_potential(self):
        	return [cell._cell.v for cell in self.all_cells]
##This returns a list of neuron ids that spiked in the last timestep
    def get_present_spiked_ids(self):
        xq = self._get_parameters("spike_times").__getitem__("spike_times").base_value
        ids = []
        #print len(xq), xq
        for i in range(0,len(xq)):
            if len(xq[i].value)!=0:
                #print xq[i].value, i
                #print "Condition met"
                #print simulator.state.t
                #print "all spikes", xq[i].value
                if simulator.state.t-1 <= xq[i].value[-1] <= simulator.state.t:
                    
                    #ids.append((i,xq[i].value[-1]))
                    ids.append(i)
        #return xq[0].value
        return ids
# You can set membrane potential during simulation.vals is a list of tuples
## [(neur_id1,voltage1),(neur_id2,voltage2)....]
    def set_membrane_potential(self, vals):
            for p in range(0,len(vals)):
                        self.all_cells[vals[p][0]]._cell.v=vals[p][1]

You can call this methods as in line 152 and 154 of the file pynnstdp.py


* Main files are PySCNN_v17.py is the latest, see other versions and for further info see their __doc__.
* poolconv.py will give mapping for a convolution in the form of an array it also has a static method for converting .aer fiels to raster plots.
* PyScoNNbackup.py setis up the populations and synspses.
* pynnstdpy.py demonstrates how to access memrbane potetnial and spike times during simulation.:

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Ruthvik Vaila, ruthvik.nitc@gmail.com
* Other community or team contact
