"""  code is similar to other PySCoNN's but the weights between ip_pop and "map1","map2".."map4" are 
different and it also plots neuron pots and it also has weight sharing mechanism, this code shares the weights of
fired neuron with other neuorns(corrected the weight sharing here) in the same map by iterating over the number of connections in the projections and also it prints the final and initial values of the evolving weights, here suppression between maps is 
neuron which fired in a map to same neuron in other maps  and also this code 
gives multiple images from the dataset as inputs not the same digit againa nd again as in previous versions
the .pkl file for allspikes is generated using code FinalData/fullspikedataset.py. This code uses customizable input maps
this code doesn't do intra map inhibition as in other codes, and it doesn't do inter map inhibition. trying to make it 
faster than v8, compared to v9, it has OFF center populations also. this code plots spikes per image per map and also
tries to enforce some maximum spikes per map per image as per saeed's suggestions, a map will be kept inactive if the
cumulative activity of the map is >average activity of other maps*homeostasis_const(over some number of images) in order
to keep all the maps in activity. Thi version is same as version 14 but it takes care of updating the weights for only
ON center maps. This removes some redundant recording which was done in previous version and it also records population
acivity for the first 500 images middle 500 images and last 500 images, this version doesn't use the
get_spike_time_ids() function which was added to pyNN core instead it sets the membrane potential of spiked neurons to
-infnitiy whihc prevents from spiking again during that image, before presenting with new image the membrane potnetials
of all the neurons are reset to resting potential. This is parallel version with spedup idea
This version uses syanpses to update weights, this is ap rallel version of working-dir/PySCNN_L34_v1.py, this version 
doesn't do the avg constant updates for maps, in v3 it's implemented."""

import pyNN.neuron as p
from pyNN.utility import ProgressBar
from poolconvclassrndm_ver2 import *
import numpy as np
import numpy
import random
from time import strftime, gmtime
from pyNN.utility import get_simulator, init_logging, normalized_filename
from pyNN.utility.plotting import DataTable
from pyNN.parameters import Sequence
from pyNN.utility.plotting import Figure, Panel
from copy import deepcopy
import sys
import pickle
import progressbar
import timeit
import operator
from pyNN import recording
from pyNN.common import control
#from mpi4py import MPI
#comm = MPI.COMM_WORLD
#start_time = timeit.default_timer()
comm,dum,ANY_SOURCE = recording.get_mpi_comm()
#from mpi4py.MPI import ANY_SOURCE

delay=1
rank = comm.Get_rank()
size = comm.Get_size()
#min_delay=1

nofMaps= 30
kernel = 5
separation = 15
nofImages= 15000
homeostasis_const= 10
activity= 0
acceltimes = 2
accel_rate= 1.6
cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 0.1,      #The refractory period in ms
                   'tau_syn_E' : 5,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 5,    #The inhibitory input current decay time-constant
                   'v_reset'   : -65,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -55  #The threshold voltage at which the neuron will spike.
                   }

p.setup(timestep=0.5,min_delay=1)#min_delay=1.25
comn_path= '/home/ruthvik/Desktop/Summer 2017/working-dir/Results/'
output = open(comn_path+'man_1spk_pool_spiketimes_focal_only_on375000_time30_mapsrank_0.pkl' ,'rb')
ipfile = comn_path+'man_1spk_pool_spiketimes_focal_only_on375000_time30_mapsrank_0.pkl'
print('picked the ip file {}').format(ipfile)
allspikes = pickle.load(output)
if(len(allspikes)!=nofMaps):
    print('Check Number of Maps, it should be equal to {}').format(len(allspikes))
    sys.exit()
output.close()
for items in allspikes.keys():
    allspikes['prev_'+str(items)]=allspikes.pop(items)


image_size= int(math.sqrt(len(allspikes['prev_map1'])))
Neurons = int(image_size*image_size) #New_image size is sqrt(no of neurons in prev layer)
prev_nofMaps = len(allspikes)

t_stop = separation*nofImages
print "TIME NEEDED FOR SIMULATION",t_stop
#100 next stage map populations.
pops = []
for i in range(1,nofMaps+1):
    pops.append("map"+str(i))

prev_pops =[]
for i in range(1,prev_nofMaps+1):
    prev_pops.append("prev_map"+str(i))

kernels ={}
for things in pops:
    for items in prev_pops:
        z = poolconv(kernel,image_size,0.8,0.05,1.0,kernel-1)
        z.PoolConv()
        kernels[items+'->'+things] =  z.conn_list 
progress_bar = ProgressBar(width=60)

# Stimulus population, used to connect to population of next stages.
stimlus_pop = {}
for items in prev_pops:
    stimlus_pop[items]=p.Population(Neurons,p.SpikeSourceArray(spike_times=\
    allspikes[items]),label=items)

#dictionary of populations for the first stage.
populations ={}
for i in range(0,len(pops)):
	populations[pops[i]] =  p.Population((image_size-kernel+1)**2, p.IF_curr_exp,cell_params_lif, label=pops[i])



#dictionary of connections from prev_population to input population L3->L4. 
ip_map_connector ={}
for things in pops:
    for items in prev_pops:
        ip_map_connector[things+str("_listconn_")+items]= p.FromListConnector(kernels[items+'->'+things], safe = True, callback = progress_bar,column_names=["weight", "delay"])

#dictionary of synapse types.
synapse_types={
               "static":p.StaticSynapse(weight=8, delay=1),
        	   "static_inh":p.StaticSynapse(weight=-1.0, delay=1),
               "custom":p.StaticSynapse(delay=1)}
#projection of input population to various maps on the first stage.
print("********Projecting input population to the {}  maps of first stage******").format(nofMaps)
projections = {}
for things in pops:
    for items in prev_pops:
	    projections[items+'->'+things] = p.Projection( stimlus_pop[items],populations[things],\
        ip_map_connector[things+str("_listconn_")+items], synapse_type = synapse_types["custom"], receptor_type="excitatory",\
        label=items+'->'+things)


print("********Done, Projecting input population to the {} maps of first stage******").format(nofMaps)

for items in populations.keys():
    populations[items].record('spikes','v')


bar = progressbar.ProgressBar(maxval=t_stop, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
start_time = timeit.default_timer()
class SparseCoding(object):
    """
    This class implements sparse coding algorithm as in Saeed's paper, There is lateral inhibition mechanism in all
    convolutionaal layers. When a neuron fires in a specific location, it inhibits other neurons in that location
    belonging to other neuronal maps and does not allow them to fire in the following time steps. In addition neurons
    are not allowed to fire more than once. A winner neuron in a map prevents other neurons in its own map to do STDP
    and duplicates its updated synaptic weights into them. Also, there is a local inter-map competition for STDP. When
    a neuron is allowed to do STDP, it prevents the neurons in other maps within a small neighbourhood around its
    location from doing STDP. THis is cruicial to encourage neurons of different maps to learn different features. 
    self.cell_voltages contains neuron voltages in a map for a time step, self.tntatv_stdp_ids contains neuron ids of
    various maps on which STDP is to be done, self.neur_ids stores all the neurons where STDP was performed and these
    neurons won't get a chance to update till the next image(list resets for ever image), self.spikes_per_img is number
    of STDPs done for one image, self.spike_rate stores the previous self.spikes_per_img value for all images."""

    def __init__(self, sampling_interval, prjctions,popltns):
        self.interval = sampling_interval
        self.prjctions = prjctions
        self.popltns = popltns
        self.cell_voltages = {}
        self.global_cell_voltages={}
        self.tntatv_stdp_ids={}
        self.global_tntatv_stdp_ids={}
        self.spikes_per_img_map_rate ={} #This is time series data of the next variable.
        self.spikes_per_map_img={}  #This is number of updates per map per img.
        for items in self.popltns.keys():
            self.spikes_per_img_map_rate[items]=[]            
            self.spikes_per_map_img[items]=0
        self.all_local_ids = {}
        self.local_ids = {}
        self.activities={}
        for items in self.popltns.keys():
            self.activities[items]=[]
            local_ids = self.popltns[items].local_cells
            self.local_ids[items] = local_ids
            all_local_ids,_,_ = recording.allgather_array(numpy.array(local_ids,dtype = 'float'))
            self.all_local_ids[items] = all_local_ids.tolist() 
        self.displaces = {}
        self.reset_disp = {} 
        self.lcl_syns = {}
        for items in self.prjctions.keys():
            weights=self.prjctions[items].get('weight',format='list',with_address=True,gather=False)
            #Sort the weights according to post syn neuron id
            weights.sort(key=lambda tup: tup[1])
            #Get all the IDs of post syn neurons in the weights acquired above.
            self.lcl_syns[items] = [weights[i][1] for i in range(0,len(weights))]
        #print self.lcl_syns[self.lcl_syns.keys()[0]], comm.rank 
        #print self.lcl_syns[self.lcl_syns.keys()[1]], comm.rank
        #print len(self.lcl_syns.keys())
        self.A_plus = 0.04
        self.A_minus = 0.03
        #self.lrate_updt_pts=[(nofImages/(acceltimes+1))*i for i in range(1,acceltimes+1)]

    def __call__(self, t):
        #Calculate the no.of input
        #print t 
        time_bin = int(t)/separation
        #if(time_bin in self.lrate_updt_pts):
        #    self.A_plus = self.A_plus*1.6
        #    self.A_minus = self.A_minus*1.6
        #Reset the threshold of populations because they might be set to infinity as per algorithm in the previous input
        if((t+0.5)%separation==0):
            #resetting the pool of previous updated neurons.
            for items in self.popltns.keys():
                self.popltns[items].set_membrane_potential([(i,cell_params_lif['v_reset']) for i in \
                range(0,self.reset_disp[items][comm.rank])])
                self.spikes_per_img_map_rate[items].append(self.spikes_per_map_img[items])
                self.spikes_per_map_img[items]=0
        
		if(time_bin>=homeostasis_const-1):
			#print 'came here'	
			cum_sum={}
			excludes=[]
			othr_map_spks=0
			for items in self.spikes_per_img_map_rate.keys(): 
				cum_sum[items]=sum(self.spikes_per_img_map_rate[items][-homeostasis_const:])
				other_maps = list(set(self.spikes_per_img_map_rate.keys())-set([items]))
				for other_items in other_maps:
					othr_map_spks+=sum(self.spikes_per_img_map_rate[other_items][-homeostasis_const:])
				
				#print('cum_sum[items]:{},avg(other_map_spks):{} in map:{}').format(cum_sum[items],othr_map_spks,items)
				#sys.exit()
				if cum_sum[items]>othr_map_spks/len(other_maps):
                    #excludes.append(items)
					for things in prev_pops:	
						synapses =list(self.prjctions[things+str("->")+items].connections)
						for  c in range(0,len(synapses)):
							synapses[c].weight = synapses[c].weight-self.A_minus*synapses[c].weight*(1-synapses[c].weight)
						#print('punished the projection {}->{}').format(things, items)
        for items in self.popltns.keys():
            zx= self.popltns[items].get_membrane_potential()
            bn,self.displaces[items], self.reset_disp[items]= recording.allgather_array(numpy.array(zx))
            self.cell_voltages[items] = zx
            self.global_cell_voltages[items]=bn.tolist()
            spiked_ids=[]
            for ids in range(0,len(self.cell_voltages[items])):
                if(self.cell_voltages[items][ids]>cell_params_lif['v_thresh']):
                    #No need to convert local_ids to global_ids here, just ids is enough.
                    #ids = self.popltns[items].id_to_index(self.local_ids[items][ids])
                    spiked_ids.append(ids)
            global_spiked_ids=[]
            for ids in range(0,len(self.global_cell_voltages[items])):
                if(self.global_cell_voltages[items][ids]>cell_params_lif['v_thresh']):
                    ids = self.popltns[items].id_to_index(self.all_local_ids[items][ids])
                    global_spiked_ids.append(ids)

            self.spikes_per_map_img[items]+=len(global_spiked_ids)
            self.tntatv_stdp_ids[items]=global_spiked_ids
            
            vals_to_set = [(vals, -float('inf')) for vals in spiked_ids]
            self.popltns[items].set_membrane_potential(vals_to_set)
        
        a_keys = self.tntatv_stdp_ids.keys()

        #Filter tentative neuron ids using suppression algo to come up with final stdp neuron ids. 
        for i in range(0,len(a_keys)):
            b_keys= list(set(a_keys) - set([a_keys[i]]))
            c_keys = self.tntatv_stdp_ids[a_keys[i]]
            for j in range(0,len(b_keys)):
                d_keys=self.tntatv_stdp_ids[b_keys[j]]
                for k in c_keys[:]:
                    key = k
                    key_row= key/(image_size-kernel+1)
                    key_col = key%(image_size-kernel+1)
                    remove =0
                    for l in d_keys[:]:
                        target = l
                        tar_row = target/(image_size-kernel+1)
                        tar_col = target%(image_size-kernel+1)
                        
                        if(abs(key_row-tar_row) > kernel-3 and abs(key_col-tar_col) > kernel-3):
                            pass
                        else:
                            if(self.global_cell_voltages[a_keys[i]][key]>=self.global_cell_voltages[b_keys[j]][target]):
                                d_keys.remove(target)
                            else:
                                remove+=1
                    if(remove):
                        c_keys.remove(key)
        
        #The value for self.tntatv_stdp_ids is same in all cores 
        # Apply STDP for the filtered tntatv_stdp_ids neuron ids.()
        
        for items in self.tntatv_stdp_ids.keys():
            #print items
            #weights=self.prjctions[str("ip->")+items].get('weight',format='list',with_address=True,gather=False)
            #Sort the weights according to post syn neuron id
            #weights.sort(key=lambda tup: tup[1])
            #Get all the IDs of post syn neurons in the weights acquired above.
            #lcl_syns = [weights[i][1] for i in range(0,len(weights))]
            a = len(self.tntatv_stdp_ids[items])
            np_zebra = np.empty(kernel**2, dtype='float')
            if(a!=0):
                volts = [self.global_cell_voltages[items][n] for n in self.tntatv_stdp_ids[items]]
                max_volt = max(volts)
                neur_id = self.tntatv_stdp_ids[items][volts.index(max_volt)]
                #if the winner neuron ID is in post syn IDs of this node update 
                #it's weights
                #print neur_id, comm.rank
                #sys.exit()
                for things in prev_pops:
                    
                    #start_time_1 = timeit.default_timer()
                    if neur_id in self.lcl_syns[things+str("->")+items]:
                         
                        weights=self.prjctions[things+str("->")+items].get('weight',format='list',with_address=True,gather=False)
                        
                        weights.sort(key=lambda tup: tup[1])
                        #uncomment the next line to see neur_id of post_syn neuron for which
                        #weights are being updated.
                        #print 'post syn neuron id',neur_id, comm.rank, items, t
                        grp_id = self.lcl_syns[things+str("->")+items].index(neur_id) #Find out at what index syns of 
                        #the post syn neuron start?
                        weights = weights[grp_id:kernel**2+grp_id]
                        #uncomment the next line to see the weights of post_syn neuron
                        #which was chosen to be updated.
                        #print 'weights, maps, time', weights, items, comm.rank, t
                        #print weights
                        
                        #print('access weights of {}->{}').format(things, items) 
                        zebra = []
                        for i in range(0,len(weights)):
                            key = weights[i][0]
                            #start_time_1 = timeit.default_timer()
                            #Q = [allspikes[things][key][x] for x in
                            #range(0,len(allspikes[things][key])) if separation*time_bin\
                            #<allspikes[things][key][x] <t]
                            #print separation*time_bin*10, t*10
                            pres = any(x in allspikes[things][key] for x in map(lambda x: x/10.0,\
                            range(int(separation*time_bin*10), int(t*10), 5))) 
                            #elapsed_1 = timeit.default_timer()-start_time_1
                            #print ('time for seeing spikes in allspikes bwn {}->{} is {} neur\
                            #{}, time_bin {}').format(things,items,elapsed_1,key,time_bin)
                            #if len(Q)!=0: 
                            if pres:
                                lst_weights = list(weights[i])
                                lst_weights[2]=lst_weights[2]+self.A_plus*lst_weights[2]*(1-lst_weights[2])
                                if(lst_weights[2]>1 or lst_weights[2]<0):
                                    sys.exit()
                                zebra.append(lst_weights[2])
                            else:
                                lst_weights = list(weights[i])
                                lst_weights[2]=lst_weights[2]-self.A_minus*lst_weights[2]*(1-lst_weights[2])
                                if(lst_weights[2]>1 or lst_weights[2]<0):
                                    sys.exit()
                                zebra.append(lst_weights[2])

                        np_zebra = np.array(zebra,dtype='float')
                        np_zebras=np_zebra.tolist()*(len(list(self.prjctions[things+str("->")+items].connections))/kernel**2)
                        synapses =list(self.prjctions[things+str("->")+items].connections) 
                        #self.prjctions[things+str("->")+items].set(weight = np_zebras)
                        for  c in range(0,len(np_zebras)):
                            synapses[c].weight = np_zebras[c]
                        all_ranks = [l for l in range(0,size)]
                        all_ranks.remove(rank)
                        for ranks in all_ranks:
                            comm.Send(np_zebra, dest=ranks)
                            #print things
                        #print 'process, weights, map,prev_map, time', comm.rank, np_zebra, items,things, t
                        #print 'bcasted weights'
                        #sys.stdout.flush()
                    else:
                        comm.Recv(np_zebra,source=ANY_SOURCE)
                        np_zebras=np_zebra.tolist()*(len(list(self.prjctions[things+str("->")+items].connections))/kernel**2)
                        #print 'length of np_zebras', len(np_zebras)
                        synapses =list(self.prjctions[things+str("->")+items].connections)
                        for  c in range(0,len(np_zebras)):
                            synapses[c].weight = np_zebras[c]
                #print ('updated all weights of {}->{}').format(things, items)
                    #elapsed_1 = timeit.default_timer()-start_time_1
                    #print ('time for the weight sharing & updating bwn {} is {}').format(t,elapsed_1)
                        #print 'shared weights, rank,map,prev_map,t', comm.rank, np_zebra,items, things, t
		if(comm.rank ==0):
			bar.update(t+self.interval-0.5)
		comm.Barrier()
        return t + self.interval 

    def get_spikes_per_img_map(self):
        return self.spikes_per_img_map_rate

sparse_coding = SparseCoding(sampling_interval=0.5, prjctions=projections, popltns = populations)
p.run(t_stop, callbacks=[sparse_coding])
bar.finish()

elapsed = timeit.default_timer()-start_time
print("Total time is {}").format(elapsed)

spikes_per_img_map = sparse_coding.get_spikes_per_img_map()

avg=0
for items in spikes_per_img_map.keys():
    avg+=sum(spikes_per_img_map[items])

print 'Avg No. of Spikes per img',avg/float(nofImages)
final_weights = {}  
for items in projections.keys():
    final_weights[items]=[]
    for i in range(0,4):
        qwer = projections[items].get('weight',format='list',with_address=True)[25*i:25*(i+1)]
        qwer.sort(key = lambda tup: tup[0])
        for j in range(0,25):
            final_weights[items].append(qwer[j][2])    

import pickle
picklefile="avg_const__weight_dict_focal_only_on"+str(t_stop)+"_time"+str(nofMaps)+"_maps"+"rank_"+str(comm.rank)+".pkl"
#picklefile1="Map_activities_focal_only_on"+str(t_stop)+"_time"+str(nofMaps)+"_maps"+".pkl"
picklefile3="avg_const_dict_spikes_per_img_map_only_on"+str(t_stop)+"_time"+str(nofMaps)+"_maps"+"rank_"+str(comm.rank)+".pkl"

path = '/home/ruthvik/Desktop/Summer 2017/working-dir/Results/'

output = open(path+picklefile,'wb')
pickle.dump(final_weights,output)
output.close()
#output1 = open(path+picklefile1,'wb')
#pickle.dump(spike_activities,output1)
#output1.close()
output3 = open(path+picklefile3,'wb')
pickle.dump(spikes_per_img_map,output3)
output3.close()
print path+picklefile
#print path+picklefile1
print path+picklefile3
comm.Barrier()
#elapsed = timeit.default_timer()-start_time
#print("Total time is {}").format(elapsed)
p.end()
