import pickle
import numpy as np
import operator
import math
import matplotlib.pyplot as plt
output = open('/home/ruthvik/Desktop/Summer 2017/working-dir/Focol_Results/6.5kImgsON_AVG_CONST_10_rate_0.04_0.03_para_4c_L4L5_vth_55/avg_const__weight_dict_focal_only_on97500_time60_mapsrank_0.pkl','rb')
final_weights = pickle.load(output)
output.close()
noOfmaps = len(final_weights.keys())/30

print "##########################################################################"


output3 = open('/home/ruthvik/Desktop/Summer 2017/working-dir/Focol_Results/6.5kImgsON_AVG_CONST_10_rate_0.04_0.03_para_4c_L4L5_vth_55/avg_const_dict_spikes_per_img_map_only_on97500_time60_mapsrank_0.pkl','rb')
spikes_per_img_map = pickle.load(output3)
output3.close()

#####This section plots Histogram of Spikes Per Image 
spikes_per_img = []

for i in range(0,len(spikes_per_img_map['map1'])): #len(spikes_per_img_map['map1']) is no.of inputs
    a=0
    for items in spikes_per_img_map.keys():
        a+=spikes_per_img_map[items][i]
    spikes_per_img.append(a)
                   

total = float(len(spikes_per_img_map.keys()))*121  #Total Number of Neurons is 60*121
spikes_per_img_rate = [items/float(total) for items in spikes_per_img] #List of Spikes/Image/Neuron


plt.xlim([min(spikes_per_img_rate)-0.5, max(spikes_per_img_rate)+0.5])
plt.hist(spikes_per_img_rate,bins=35)
plt.title('Histogram of the spikes per img')
plt.xlabel('Spikes per image per neuron')
plt.ylabel('count')
plt.show()
#Scatter plot of spikes per img VS time
times = [i for i in range(0,len(spikes_per_img))]
plt.scatter(times,spikes_per_img)
plt.xlabel('No.Of Images')
plt.ylabel('Spikes per img')
plt.show()
############## This section plots Histogram of Spikes per Map##################
spikes_per_map =[]   ###This represents total number of spikes in a map
for items in spikes_per_img_map.keys():
	#print items
	spikes_per_map.append(sum(spikes_per_img_map[items][:]))

plt.xlim([min(spikes_per_map)-5, max(spikes_per_map)+5])
plt.hist(spikes_per_map,bins=60)
plt.title('Histogram of the spikes per map')
plt.xlabel('Spikes per map')
plt.ylabel('count')
plt.show()

##############This section plots scatter plot of spikes per map with labels########
spikes_per_maps = {}
for items in spikes_per_img_map.keys():
    spikes_per_maps[items]=sum(spikes_per_img_map[items][:])
    
sorted_spikes_per_maps = sorted(spikes_per_maps.items(), key=operator.itemgetter(1))
#print sorted_spikes_per_maps

keys = spikes_per_maps.keys()
map_spikes = [spikes_per_maps[items] for items in keys]
mapids=[items.strip('map') for items in keys]
mapids=[int(items)  for items in mapids]

plt.scatter(mapids,map_spikes)
y_line = [np.mean(map_spikes)]*len(keys)
x_line = np.arange(noOfmaps)
plt.plot(x_line,y_line,'g-',label="Mean")
up_y_line = [np.mean(map_spikes)+ math.sqrt(np.var(map_spikes))]*len(keys)
down_y_line = [np.mean(map_spikes)- math.sqrt(np.var(map_spikes))]*len(keys)
plt.plot(x_line,up_y_line,'b-',label="1 SD above")
plt.plot(x_line,down_y_line,'r-',label="1 SD below")
plt.legend()
plt.xlabel('Map')
plt.ylabel('Spikes per map')

for label, x, y in zip(keys, mapids, map_spikes):
    plt.annotate(
        label,
        xy=(x, y), xytext=(-20, 20),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
    
plt.show()

ones=0
Partici_maps = 0
for i in range(1,61):
    print 'prev_map1->map'+str(i)
    print final_weights['prev_map1->map'+str(i)][0:25]
    print final_weights['prev_map1->map'+str(i)][0:25]==final_weights['prev_map1->map'+str(i)][25:50]
    if(final_weights['prev_map1->map'+str(i)][0:25]==final_weights['prev_map1->map'+str(i)][25:50]):
        Partici_maps+=1
        for j in range(1,31):
            zx = max(final_weights['prev_map'+str(j)+'->map'+str(i)][0:25])#uncomment this to see if unbalanced
            if(zx==float('Inf') or zx<0):
               print final_weights['prev_map'+str(j)+'->map'+str(i)][0:50]
               sys.exit()
            elif(zx>=0.6):
                ones+=1
            print('max of {} is {}').format('prev_map'+str(j)+'->map'+str(i), zx)
            #print '####################################'
            qw=min(final_weights['prev_map'+str(j)+'->map'+str(i)][0:25])
            print('min is{}').format(qw) 

print('{} Maps that took part in weight updates').format(Partici_maps)
print('Number weights that are greater than 0.6 is {}').format(ones)
        
