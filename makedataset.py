import pickle
import numpy as np
import operator
import math
import matplotlib.pyplot as plt
from poolconvclassrndm_ver2 import *
noOfImages = 5000 ##Make sure that you enter correct number here. No of Rows will give number of training samples
##Final dataset has both data and labels, first element in numpy array is data and second is labels.
##Don't forget to change train or test and do the same for in picklefile also.
path = '/home/ruthvik/Desktop/Summer 2017/working-dir/Focol_Results/Training'
output = open(path+'/train_man_1spk_pool_voltages_L4_focal_only_on75000_time30_mapsrank_0.pkl','rb')
voltages = pickle.load(output)
output.close()

output1 = open(path+'/train_labels25000.pkl','rb')
labels = pickle.load(output1)
output.close()

dataset = [[] for i in range(noOfImages)]
for i in range(0,noOfImages):
    for j in range(1,len(voltages)+1):
        dataset[i].append(voltages['map'+str(j)][i])
    

#dataset = np.array(dataset)
#labels = np.array(labels[0:noOfImages])
labels = labels[0:noOfImages]


datanlabels = np.array([dataset,labels])

picklefile="train_data_labels"+str(noOfImages)+".pkl"
path = '/home/ruthvik/Desktop/Summer 2017/working-dir/Focol_Results/Training/'
output = open(path+picklefile,'wb')
pickle.dump(datanlabels,output)
print "pickle file written to",path+picklefile

