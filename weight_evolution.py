import pickle
import numpy as np
import operator
import math
import matplotlib.pyplot as plt
noOfImages = 15000
interval = 2500
N=noOfImages/interval
images = [2500*i for i in range(0,N+1)]
weight_evols = []
path = '/home/ruthvik/Desktop/Focol_Results/Serial_15000_imgs_progress_test_man_OFF_1tstep_1half_hr/'
for items in images:
    
    output = open(path+'avg_const_weight_dict_manual_only_off_1tstepImg'+str(items)+'_time30_mapsrank_0.pkl','rb')
    weights = pickle.load(output)
    weight_evols.append(weights)
    output.close()
    
noOfmaps = len(weight_evols[0].keys())
print noOfmaps

print "##########################################################################"
maps=weight_evols[0].keys()

print maps

fig, axes = plt.subplots(len(images), noOfmaps,figsize=(30, 4),
                         subplot_kw={'xticks': [], 'yticks': []})
#print axes
#fig.tight_layout()
#fig.subplots_adjust(hspace=0, wspace=0.7)



for i in range(len(weight_evols)):
    az = axes.flat[noOfmaps*i:noOfmaps*(i+1)]
    for ax, filters in zip(az, maps):
        x = weight_evols[i][filters][0:25]
        ax.imshow(np.array(x).reshape(5,5),cmap='gray',interpolation='none')
        ax.set_title(filters.strip('ip->map'))

plt.show()
