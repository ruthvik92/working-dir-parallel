
"""In this code there is no learning it just forms L1(1map)->L2(30maps,convolution)->L3(30maps,pooling) and records
spiketimes for L3.  In this code neuron voltages for every image is reset unline in v1."""

import pyNN.neuron as p
from pyNN.utility import ProgressBar
from poolconvclassrndm_ver2 import *
import numpy as np
import random
from time import strftime, gmtime
from pyNN.utility import get_simulator, init_logging, normalized_filename
from pyNN.utility.plotting import DataTable
from pyNN.parameters import Sequence
from pyNN.utility.plotting import Figure, Panel
from copy import deepcopy
import sys
import pickle
import progressbar
import timeit
import operator
from pyNN import recording
from pyNN.common import control
comm,dum,ANY_SOURCE = recording.get_mpi_comm()
delay=1
rank = comm.Get_rank()
size = comm.Get_size()
#min_delay=1

L2maps= 30
L4maps = 60
kernel = 5
separation = 15
nofImages= 25000
pool_kernel = 2
image_size =27
Neurons = image_size*image_size
cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 0.1,      #The refractory period in ms
                   'tau_syn_E' : 5,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 5,    #The inhibitory input current decay time-constant
                   'v_reset'   : -65,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -58  #The threshold voltage at which the neuron will spike.
                   }

p.setup(timestep=0.5,min_delay=1)#min_delay=1.25

############################## Loading the input spike file for L1########################################
ip_spikes = open('/home/ruthvik/Desktop/Summer \
2017/working-dir/Focol_Stimulus/stimulus6000030_separationImagesfocol_rate7_width1_sigma2_types1.pkl','rb')
allspikes = pickle.load(ip_spikes)
allspikes = [(np.array(allspikes[i])/2.0).tolist() for i in range(0,len(allspikes))]
###Since Neuron and PyNN have an issue with min_delay and timestep, converted all 1ms timestep to 0.5ms by divind it by2
ip_spikes.close()

############################# Loading the trained weight file for L1-L2####################################
weights1 = open('/home/ruthvik/Desktop/Summer \
2017/working-dir/Focol_Results/25kImgsON_AVG_CONST_10_rate_0.03_0.04_para_4c/avg_const__weight_dict_focal_only_on375000_time30_mapsrank_0.pkl','rb')
L1_weights_L2=pickle.load(weights1)
weights1.close()  ###DONT FORGET TO CHANGE THE KEYS

############################# Loading the trained weight file for L3-L4####################################
weights2 = open('/home/ruthvik/Desktop/Summer \
2017/working-dir/Focol_Results/25kImgsON_AVG_CONST_10_rate_0.03_0.04_para_4c/avg_const__weight_dict_focal_only_on375000_time30_mapsrank_0.pkl','rb')
L3_weights_L4=pickle.load(weights2)
weights2.close() #####DONT FORGET TO CHANGE THE KEYS


#############################Calculate the total biological time for the sim##############################
#t_stop =int( max([max(items) for items in allspikes if len(items)!=0])+5)
t_stop = separation*nofImages
print "TIME NEEDED FOR SIMULATION",t_stop


############################Create the labels for the populations in (L2)##############################
L2pops = []
for i in range(1,L2Maps+1):
    L2pops.append("L2map"+str(i))


###########################Create the labels for the populations in (L3)#############################
L3pops =[]
for i in range(1,L2maps+1):
    L3pops.append("L3map"+str(i))

############################Create the labels for the populations in (L4)##############################
L4pops = []
for i in range(1,L4maps+1):
    L4pops.append("L4map"+str(i))


############################Create the labels for the populations in (L5)##############################
L5pops = []
for i in range(1,L4maps+1):
    L5pops.append("L5map"+str(i))

###########################Initialize kernels for L1-L2 projections ####################################
L1_kernels_L2 ={}
for i in range(0,len(L2pops)):
    z = poolconv(kernel,27,0.8,0.05,1.0,4)
    z.PoolConv()
    L1_kernels_L2[str(L1)+'->'+L2pops[i]] =  z.conn_list

##########################Re-write the initialized kenels with the stored L1_weights_L2##################
for maps in L1_weights_L2.keys():
    map_weights=L1_weights_L2[maps][0:kernel**2]*(len(L1_kernels_L2[str(L1)+'->'+maps])/kernel**2)
    for i in range(0,len(L1_kernels_L2[str(L1)+'->'+maps])):
        lst_weight =list(L1_kernels_L2[str(L1)+'->'+maps][i])
        lst_weight[2]=map_weights[i]
        tuple_weight = tuple(lst_weight)
        L1_kernels_L2[str(L1)+'->'+maps][i]=tuple_weight


#########################Initialize the pooling kernels for L2 to L3########################################
L2_kernels_L3 ={}
for i in range(0,len(L3pops)):
    pool_z = poolconv(pool_kernel,pool1_size-kernel+1,1,0,1.0,1)
    pool_z.PoolConv()
    L2_kernels_L3[L2pops[i]+str('->')+L3pops[i]] =  pool_z.conn_list 

###########################Initialize kernels for L3-L4 projections ###############################
L3_kernels_L4 ={}
for things in L4pops:
    for items in L3pops:
        z = poolconv(kernel,pool1_size,0.8,0.05,1.0,kernel-1)
        z.PoolConv()
        L3_kernels_L4[items+'->'+things] =  z.conn_list

##########################Re-write the initialized kernels with the stored L3_weights_L4###########
for maps in L3_weights_L4.keys():
    map_weights=L3_weights_L4[maps][0:kernel**2]*(len(L3_kernels_L4[maps])/kernel**2)
    for i in range(0,len(L3_kernels_L4[maps])):
        lst_weight =list(L3_kernels_L4[maps][i])
        lst_weight[2]=map_weights[i]
        tuple_weight = tuple(lst_weight)
        L3_kernels_L4[maps][i]=tuple_weight


#########################Initialize the pooling kernels for L4 to L5########################################
L4_kernels_L5 ={}
for i in range(0,len(L5pops)):
    pool_z = poolconv(pool_kernel,pool1_size-kernel+1,1,0,1.0,1)
    pool_z.PoolConv()
    L4_kernels_L5[L4pops[i]+'->'+L5pops[i]] =  pool_z.conn_list 


######################## Create the stimulus L1 population ######################################################
L1population = p.Population(Neurons,p.SpikeSourceArray(spike_times= allspikes),label="driver")


####################### Create the convolving L2 populations #####################################################
L2populations ={}
for i in range(0,len(L2pops)):
    L2populations[L2pops[i]] =  p.Population((image_size-kernel+1)**2, p.IF_curr_exp,cell_params_lif, label=L2pops[i])

#########################Creating the pooling population L3###############################################
L3populations = {}
for items in L3pops:
	L3populations[items]=p.Population(((image_size-kernel+1)/pool_kernel)**2, p.IF_curr_exp,cell_params_lif, label=items)

pool1_size = ((image_size-kernel+1)/pool_kernel)**2

#########################Creating the convolving populations L4################################################
L4populations ={}
for i in range(0,len(L4pops)):
	L4populations[L4pops[i]] =  p.Population((pool1_size-kernel+1)**2, p.IF_curr_exp,cell_params_lif, label=pops[i])


#########################Creating the Pooling population L5###############################################
L5populations ={}
for i in range(0,len(L5pops)):
	L5populations[L5pops[i]] =  p.Population(((pool1_size)/pool_kernel)**2, p.IF_curr_exp,cell_params_lif, label=pops[i])

progress_bar = ProgressBar(width=60)

#########################Creating the connectors for L1-L2 projections ##################################
L1L2connector ={}
for i in range(0,len(L2pops)):
    L1L2connector[str("L1_listconn_")+L2pops[i]]= p.FromListConnector(L1_kernels_L2[L2pops[i]], safe = True, callback = progress_bar,column_names=["weight", "delay"])

#########################Creating the connectors for L2-L3 projections #################################
L2L3connector ={}
for i in range(0,len(L2pops)):
    L2L3connector[L2pops[i]+str("listconn_")+L3pops[i]]= p.FromListConnector(L2_kernels_L3[pops[i]], safe = True, callback = progress_bar,column_names=["weight", "delay"])

#########################Creating the connectors for L3 to L4############################################
L3L4connector ={}
for things in L4pops:
    for items in L3pops:
        L3L4connector[items+str("_listconn_")+things]= p.FromListConnector(L3_kernels_L4[items+'->'+things], safe = True, callback = progress_bar,column_names=["weight", "delay"])

#########################Creating the connectors for L4 to L5############################################
L4L5connector ={}
for i in range(0,len(L4pops)):
    L4L5connector[L4pops[i]+str("listconn_")+L5pops[i]]= p.FromListConnector(L4_kernels_L5[L4pops[i]], safe = True, callback = progress_bar,column_names=["weight", "delay"])

###########################Create a dictionary of useful synapses ######################################
synapse_types={
               "static":p.StaticSynapse(weight=8, delay=1),
               "static_inh":p.StaticSynapse(weight=-1.0, delay=1),
               "custom":p.StaticSynapse(delay=1)}

###########################Creating L1 to L2 projections ###############################################
L1L2projections = {}
for i in range(0,len(L2pops)):
    L1L2projections[str("L1->")+L2pops[i]] = p.Projection(L1population,L2populations[L2pops[i]],\
    L1L2connector[str("L1_listconn_")+L2pops[i]], synapse_type = synapse_types["custom"], receptor_type="excitatory")

###########################Creating L2 to L3 projections ###############################################
L2L3projections = {}
for i in range(0,len(L3pops)):
    L2L3projections[L2pops[i]+str('->')+L3pops[i]] = p.Projection(L2populations[L2pops[i]],L3populations[L3pops[i]],\
    L2L3connector[L2pops[i]+str("listconn_")+L3pops[i]], synapse_type = synapse_types["custom"], receptor_type="excitatory")

##########################Creating the projections from L3 to L4 #######################################
L3L4projections = {}
for things in L4pops:
    for items in L3pops:
        L3L4projections[items+'->'+things] = p.Projection( L3populations[items],L4populations[things],\
        L3L4connector[items+str("_listconn_")+things], synapse_type = synapse_types["custom"], receptor_type="excitatory",\
        label=items+'->'+things)
#########################Creating the projections from L4 to L5 ########################################
L4L5projections = {}
for i in range(0,len(L5pops)):
    L4L5projections[L4pops[i]+str("->")+L5pops[i]] = p.Projection(L4populations[L4pops[i]],L5populations[L5pops[i]],\
    L4L5connector[L4pops[i]+str("listconn_")+L5pops[i]], synapse_type = synapse_types["custom"], receptor_type="excitatory")


for items in L5populations.keys():
    L5populations[items].record('spikes','v')

bar = progressbar.ProgressBar(maxval=t_stop, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
start_time = timeit.default_timer()

class MyReset(object): 
    def __init__(self, sampling_interval, popltns1, popltns2):	
        self.interval = sampling_interval
        self.popltns1 = popltns1
		self.popltns2 = popltns2 
        self.cell_voltages={}
		self.cell_voltages2 ={}
        self.global_cell_voltages={}
		self.global_cell_voltages2={}
        self.displaces={}
		self.displaces2={}
        self.reset_disp={}
		self.reset_disp2={}
        self.all_local_ids = {}
		self.all_local_ids2 ={}
        self.local_ids={}
		self.local_ids2={}
        self.pool_spiketimes={}
		self.pool_spiketimes2={}
        for items in self.popltns1.keys():
            local_ids = self.popltns1[items].local_cells
            self.local_ids[items] = local_ids
            all_local_ids,_,_ = recording.allgather_array(np.array(local_ids,dtype = 'float'))
            self.all_local_ids[items] = all_local_ids.tolist()
            self.pool_spiketimes[items] = [[] for i in range(121)]
		
        for items in self.popltns2.keys():
            local_ids2 = self.popltns2[items].local_cells
            self.local_ids2[items] = local_ids2
            all_local_ids2,_,_ = recording.allgather_array(np.array(local_ids2,dtype = 'float'))
            self.all_local_ids2[items] = all_local_ids2.tolist()
            self.pool_spiketimes2[items] = [[] for i in range(9)]
    def __call__(self,t):
        ##Reset the voltages of neurons after passing an image
        if(t!=0):
            if((t)%separation==0):
                #print 'came here and time is',t
                for items in self.popltns1.keys():
                    self.popltns1[items].set_membrane_potential([(i,cell_params_lif['v_reset']) for i in \
                    range(0,self.reset_disp[items][comm.rank])])
					
                for items in self.popltns2.keys():
                    self.popltns2[items].set_membrane_potential([(i,cell_params_lif['v_reset']) for i in \
                    range(0,self.reset_disp2[items][comm.rank])])	
		##This pasrt of code is to prevent more than one spike by a neuron in entire duration of an image	
        for items in self.popltns1.keys():
            zx= self.popltns1[items].get_membrane_potential()
            bn,self.displaces[items], self.reset_disp[items]= recording.allgather_array(np.array(zx))
            self.cell_voltages[items] = zx
            self.global_cell_voltages[items]=bn.tolist()
            spiked_ids=[]
            for ids in range(0,len(self.cell_voltages[items])):
                if(self.cell_voltages[items][ids]>cell_params_lif['v_thresh']):
                    #No need to convert local_ids to global_ids here, just ids is enough.
                    #ids = self.popltns[items].id_to_index(self.local_ids[items][ids])#id_to_index method converts 
                    #local ids to global ids
                    spiked_ids.append(ids)
                    #print('cell {} in {} spiked, voltage {},global id {} rank {},time {}').format(ids,\
                    #items,self.cell_voltages[items][ids],self.popltns[items].id_to_index(self.local_ids[items][ids]),comm.rank,t)
            
            #print('cells {} in {} spiked, rank {},time {}').format(spiked_ids, items, comm.rank,t)
            global_spiked_ids=[]
            for ids in range(0,len(self.global_cell_voltages[items])):
                if(self.global_cell_voltages[items][ids]>cell_params_lif['v_thresh']):
                    ids = self.popltns1[items].id_to_index(self.all_local_ids[items][ids])
                    global_spiked_ids.append(ids)
            for nos in global_spiked_ids:
                self.pool_spiketimes[items][nos].append(t)
            
            
            #print('global cells {} in {} spiked rank {} time {}').format(global_spiked_ids, items, comm.rank,t)
            #if(t==10.0):
            #    sys.exit()
            vals_to_set = [(vals, -float('inf')) for vals in spiked_ids]
            self.popltns1[items].set_membrane_potential(vals_to_set)
			
        for items in self.popltns2.keys():
            zx= self.popltns2[items].get_membrane_potential()
            bn,self.displaces2[items], self.reset_disp2[items]= recording.allgather_array(np.array(zx))
            self.cell_voltages2[items] = zx
            self.global_cell_voltages2[items]=bn.tolist()
            spiked_ids=[]
            for ids in range(0,len(self.cell_voltages2[items])):
                if(self.cell_voltages2[items][ids]>cell_params_lif['v_thresh']):
                    #No need to convert local_ids to global_ids here, just ids is enough.
                    #ids = self.popltns[items].id_to_index(self.local_ids[items][ids])#id_to_index method converts 
                    #local ids to global ids
                    spiked_ids.append(ids)
                    #print('cell {} in {} spiked, voltage {},global id {} rank {},time {}').format(ids,\
                    #items,self.cell_voltages[items][ids],self.popltns[items].id_to_index(self.local_ids[items][ids]),comm.rank,t)
            
            #print('cells {} in {} spiked, rank {},time {}').format(spiked_ids, items, comm.rank,t)
            global_spiked_ids=[]
            for ids in range(0,len(self.global_cell_voltages2[items])):
                if(self.global_cell_voltages2[items][ids]>cell_params_lif['v_thresh']):
                    ids = self.popltns2[items].id_to_index(self.all_local_ids2[items][ids])
                    global_spiked_ids.append(ids)
            for nos in global_spiked_ids:
                self.pool_spiketimes2[items][nos].append(t)
            
            
            #print('global cells {} in {} spiked rank {} time {}').format(global_spiked_ids, items, comm.rank,t)
            #if(t==10.0):
            #    sys.exit()
            vals_to_set = [(vals, -float('inf')) for vals in spiked_ids]
            self.popltns2[items].set_membrane_potential(vals_to_set)
	
        if(comm.rank ==0):
            bar.update(t+self.interval-0.5)
        comm.Barrier()
        return t + self.interval

    def GetSpikes(self):
        return self.pool_spiketimes2

my_reset = MyReset(sampling_interval=0.5, popltns1=L3populations, popltns2 = L5populations)
p.run(t_stop,callbacks=[my_reset])
bar.finish()
elapsed = timeit.default_timer()-start_time
print("Total time is {}").format(elapsed)
spikes = my_reset.GetSpikes()
pool_spike_times={}
for pops in pool_populations.keys():
	pool_spike_times[pops]=pool_populations[pops].get_data(gather=True).segments[0].spiketrains

path = '/home/ruthvik/Desktop/Summer 2017/working-dir/Results/'
#picklefile2="auto_1spk_pool_spiketimes_focal_only_on"+str(t_stop)+"_time"+str(nofMaps)+"_maps"+"rank_"+str(comm.rank)+".pkl"
#spike_times = open(path+picklefile2,'wb')
#pickle.dump(pool_spike_times,spike_times)
#spike_times.close()
#print path+picklefile2
if(comm.rank==0):
    picklefile3="man_1spk_pool_spiketimes_focal_only_on"+str(t_stop)+"_time"+str(nofMaps)+"_maps"+"rank_"+str(comm.rank)+".pkl"
    spike_times = open(path+picklefile3,'wb')
    pickle.dump(spikes,spike_times)
    spike_times.close()
    print path+picklefile3
comm.Barrier()
p.end()
