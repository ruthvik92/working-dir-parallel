import pickle
import numpy as np
import operator
import math
import matplotlib.pyplot as plt
output = open('/home/ruthvik/Desktop/Focol_Results/Serial_7500_imgs_progress_test_man_7hrs_OFF_roundoff/avg_const__weight_dict_manual_only_off0_time30_mapsrank_0.pkl','rb')
final_weights = pickle.load(output)
output.close()
noOfmaps = len(final_weights.keys())

print "##########################################################################"
maps=final_weights.keys()

plt.subplot(1,2,1)
fig, axes = plt.subplots(5, 6, figsize=(8, 8),
                         subplot_kw={'xticks': [], 'yticks': []})
#print axes

fig.subplots_adjust(hspace=0.3, wspace=0.05)


for ax, filters in zip(axes.flat, maps):
    x = final_weights[filters][0:25]
    ax.imshow(np.array(x).reshape(5,5),cmap='gray',interpolation='none')
    ax.set_title(filters.strip('ip->'))

plt.show()
